//
// Created by jlampertz on 18/07/2023.
//

#include "Vector2D.h"
#include <cmath>

const Vector2D Vector2D::ZERO{0, 0};

float Vector2D::GetLength() const
{
    return (float) std::sqrt(X * X + Y * Y);
}

Vector2D Vector2D::Normalize() const
{
    float length = this->GetLength();
    return Vector2D{X / length, Y / length};
}
