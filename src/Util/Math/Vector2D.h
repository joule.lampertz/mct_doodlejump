//
// Created by jlampertz on 18/07/2023.
//

#pragma once

/**
 * Vector2D represents 2 dimensional vectors and implements some math operations around them.
 */
struct Vector2D
{
public:
    static const Vector2D ZERO;

    /**
     * first element of the vector.
     */
    float X;
    /**
     * second element of the vector.
     */
    float Y;

    /**
     * @return length of the vector.
     */
    [[nodiscard]]
    float GetLength() const;

    /**
     * @return the unit vector of this vector.
     */
    [[nodiscard]]
    Vector2D Normalize() const;

    inline bool operator==(const Vector2D &other) const
    {
        return this->X == other.X && this->Y == other.Y;
    }

    inline bool operator!=(const Vector2D &other) const
    {
        return !(*this == other);
    }

    inline Vector2D &operator+=(const Vector2D &other)
    {
        this->X += other.X;
        this->Y += other.Y;
        return *this;
    }

    inline Vector2D &operator-=(const Vector2D &other)
    {
        this->X -= other.X;
        this->Y -= other.Y;
        return *this;
    }

    inline Vector2D &operator*=(float factor)
    {
        this->X *= factor;
        this->Y *= factor;
        return *this;
    }

    inline friend Vector2D operator+(Vector2D l, const Vector2D &r)
    {
        l += r;
        return l;
    }

    inline friend Vector2D operator-(Vector2D l, const Vector2D &r)
    {
        l -= r;
        return l;
    }

    inline friend Vector2D operator*(Vector2D vector, float factor)
    {
        vector *= factor;
        return vector;
    }

    inline friend Vector2D operator*(float factor, Vector2D vector)
    {
        vector *= factor;
        return vector;
    }
};
