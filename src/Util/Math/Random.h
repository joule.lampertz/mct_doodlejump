//
// Created by jlampertz on 23/07/2023.
//

#pragma once


#include <utility>

/**
 * static class that contains function for generate different kinds of pseudo random value.
 */
class Random
{
private:
    /** whether a seed has been set yet. */
    static bool _seeded;

    /**
     * sets a (predefined) seed if none has been set yet.
     */
    static void init();

public:
    /**
     * sets the seed for the random generation.
     * @param seed the seed used for the random generation
     */
    static void SetSeed(unsigned int seed);

    /**
     * generate a random int between a min and a max value.
     * the output is pseudo-random and might not follow a perfect normal distribution.
     * @param min minimum (included)
     * @param max maximum (excluded)
     * @return the generated value
     */
    static int GenerateInt(int min, int max);

    /**
     * selects a weighted random value from the input array of values and their weights.
     + the output is pseudo-random and might not follow a perfect normal distribution.
     * @param valueWeightPairs array of value and weight pairs
     * @param length length of the input array
     * @return a weighted random value from the input array
     */
    static int GenerateWeightedInt(const std::pair<int, float> *valueWeightPairs, unsigned short length);

    /**
     * generates a random float between 0 (included) and 1 (excluded).
     * the output is pseudo-random and might not follow a perfect normal distribution.
     * @return the generated float
     */
    static float GenerateFloat();
};
