//
// Created by jlampertz on 23/07/2023.
//

#include <cstdlib>
#include "Random.h"
#include "yahal_assert.h"

bool Random::_seeded = false;


void Random::init()
{
    if (!_seeded) SetSeed(20);
}

void Random::SetSeed(unsigned int seed)
{
    // i know rand() isn't perfect, but it will do for this project
    srand(seed);
    _seeded = true;
}

int Random::GenerateInt(int min, int max)
{
    init();
    return min + (rand() % std::abs(max - min));
}

float Random::GenerateFloat()
{
    init();
    return (float) (rand() % 32767) / 32766.f;
}

int Random::GenerateWeightedInt(const std::pair<int, float> *valueWeightPairs, unsigned short length)
{
    float sum = 0;
    for (unsigned short i = 0; i < length; i++)
    {
        sum += valueWeightPairs[i].second;
    }

    float rand = GenerateFloat();

    float offset = 0;
    for (unsigned short i = 0; i < length; i++)
    {
        float weight = offset + (valueWeightPairs[i].second / sum);
        if (weight > rand) return valueWeightPairs[i].first;

        offset = weight;
    }

    yahal_assert(false);
    return 0;
}
