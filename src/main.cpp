#include "gpio_msp432.h"
#include "spi_msp432.h"
#include "task.h"
#include "Game/Game.h"
#include "Hardware/ConsoleOutput.h"

#define SPI_IF  EUSCI_B3_SPI

int main()
{

    Game *game = Game::GetInstance();
    game->AddLoggerOutput(new ConsoleOutput{});
    game->InitComponents();
    game->StartGame();

    return 0;
}
