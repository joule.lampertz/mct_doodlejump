//
// Created by jlampertz on 26/06/2023.
//

#include "DisplayMatrix.h"

void DisplayMatrix::DrawSprite(const Sprite &sprite, int posX, int posY, bool transparent)
{
    // don't draw if nothing is visible
    if (posX >= WIDTH || posY >= HEIGHT || posX + sprite.Width < 0 || posY + sprite.Height < 0)
        return;

    // cut off parts outside camera view
    unsigned short startX = posX >= 0 ? 0 : (unsigned short) -posX;
    unsigned short startY = posY >= 0 ? 0 : (unsigned short) -posY;
    unsigned short endX = posX + sprite.Width < WIDTH ?
                          sprite.Width :
                          WIDTH - (unsigned short) posX;
    unsigned short endY = posY + sprite.Height < HEIGHT ?
                          sprite.Height :
                          HEIGHT - (unsigned short) posY;

    for (unsigned short y = startY; y < endY; ++y)
    {
        for (unsigned short x = startX; x < endX; ++x)
        {
            if (transparent && sprite.TransparentColor == sprite.GetPixel(x, y)) continue; // ignore transparent pixel
            setPixel(posX + x, posY + y, sprite.GetPixel(x, y));
        }
    }
}

void DisplayMatrix::Fill(Color16b color)
{
    std::fill_n(_matrix, WIDTH * HEIGHT, color);
}

DisplayMatrix::DisplayMatrix()
{
    Bitmap = new uGUI::BMP{};
    Bitmap->width = WIDTH;
    Bitmap->height = HEIGHT;
    Bitmap->bpp = BitsPerPixel;
    Bitmap->colors = BMP_RGB565;
    Bitmap->p = _matrix;
}

DisplayMatrix::~DisplayMatrix()
{
    delete Bitmap;
}

void DisplayMatrix::setPixel(unsigned short x, unsigned short y, Color16b color)
{
    if (x >= WIDTH || y >= HEIGHT) return;
    _matrix[(HEIGHT - y - 1) * WIDTH + x] = color;
}
