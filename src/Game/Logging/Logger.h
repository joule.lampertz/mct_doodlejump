//
// Created by jlampertz on 27/06/2023.
//

#pragma once


#include <set>
#include "ILoggerOutput.h"

/**
 * the logger class allows logging to messages to attached logger outputs.
 */
class Logger
{
private:
    std::set<ILoggerOutput *> _outputs;

public:
    /** creates a new logger object. */
    Logger();

    /** adds a logger output. */
    void AddOutput(ILoggerOutput *output);

    /** removes a given logger output. */
    void RemoveOutput(ILoggerOutput *output);

    /** logs a message to all attached logger outputs */
    void Log(const char *msg, LogLevel level = DEFAULT);
};
