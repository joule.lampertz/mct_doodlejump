//
// Created by jlampertz on 27/06/2023.
//

#include "Logger.h"

Logger::Logger()
        : _outputs()
{

}

void Logger::AddOutput(ILoggerOutput *output)
{
    _outputs.insert(output);
}

void Logger::RemoveOutput(ILoggerOutput *output)
{
    _outputs.erase(output);
}

void Logger::Log(const char *msg, LogLevel level)
{
    for (ILoggerOutput *output: _outputs)
    {
        output->Log(msg, level);
    }
}
