//
// Created by jlampertz on 27/06/2023.
//

#pragma once

#include <string>

enum LogLevel : char
{
    DEFAULT = 0,
    SUCCESS,
    WARNING,
    ERROR
};

/**
 * the ILoggerOutput interface can be implemented to allow a class to be used as output for logs.
 */
class ILoggerOutput
{
public:
    virtual ~ILoggerOutput() = default;

    /**
     * logs a message to this output.
     * @param msg log message.
     * @param level log level (used for formatting and such).
     */
    virtual void Log(const char *msg, LogLevel level) = 0;
};
