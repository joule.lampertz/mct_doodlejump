//
// Created by jlampertz on 23/06/2023.
//

#pragma once

#include "uGUI.h"

/** 16bit RGB565 colour */
typedef unsigned short Color16b;

class ColorRGB565
{
public:
    static const unsigned char MaxValRed = 31;
    static const unsigned char MaxValGreen = 62;
    static const unsigned char MaxValBlue = 31;

    // some predefined colours
    static const Color16b WHITE = 0xFFFF;
    static const Color16b BLACK = 0x0000;
    static const Color16b RED = 0xF800;
    static const Color16b GREEN = 0x07E0;
    static const Color16b BLUE = 0x001F;
    static const Color16b YELLOW = 0xFFE0;
    static const Color16b MAGENTA = 0xF81F;
    static const Color16b CYAN = 0x07FF;
    static const Color16b SKY_BLUE = 0x7F7F;

    /**
     * creates a new 16b rgb565 colour from its individual red, green and blue values
     * @param red between 0 and 31.
     * @param green between 0 and 62.
     * @param blue between 0 and 31.
     * @return new colour.
     */
    static Color16b Create16(unsigned char red, unsigned char green, unsigned char blue);

    /**
     * creates a new 16b rgb565 colour in 32 bit formatting from its individual red, green and blue values
     * @param red between 0 and 31.
     * @param green between 0 and 62.
     * @param blue between 0 and 31.
     * @return new colour.
     */
    static UG_COLOR Create32(unsigned char red, unsigned char green, unsigned char blue);

    /**
     * converts a 16bit  formatted RGB565 colour to a 32bit formatted RGB565 colour.
     * @param color 16bit RGB565 colour.
     * @return corresponding 32bit RGB565 colour.
     */
    static UG_COLOR Convert16To32(Color16b color);


private:
    /** Bitmask used by uGUI to identify 32bit RGB565 colours */
    static const UG_COLOR RGB565BitMask = 0x01000000;
};
