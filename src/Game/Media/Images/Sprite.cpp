//
// Created by jlampertz on 23/06/2023.
//

#include <cstdio>
#include "Sprite.h"

Sprite::Sprite(short width, short height, Color16b transparentColor)
{
    Width = width;
    Height = height;
    TransparentColor = transparentColor;
    Bitmap = new Color16b[width * height]{};
}

Sprite::Sprite(Color16b *bitmap, unsigned short width, unsigned short height, Color16b transparentColor = 0)
{
    Width = width;
    Height = height;
    TransparentColor = transparentColor;
    Bitmap = bitmap;
}

Sprite::Sprite(const Sprite &sprite)
{
    this->Width = sprite.Width;
    this->Height = sprite.Height;
    this->TransparentColor = sprite.TransparentColor;
    this->Bitmap = new Color16b[sprite.Width * sprite.Height]{};
    std::copy(sprite.Bitmap, sprite.Bitmap + (sprite.Width * sprite.Height - 1), this->Bitmap);
}

Sprite::~Sprite()
{
    delete[] Bitmap;
}

void Sprite::SetPixel(unsigned short posX, unsigned short posY, Color16b color)
{
    Bitmap[(Height - posY - 1) * Width + posX] = color;
}


Color16b Sprite::GetPixel(unsigned short posX, unsigned short posY) const
{
    return Bitmap[(Height - posY - 1) * Width + posX];
}

void Sprite::FlipHorizontally()
{
    unsigned short halfWidth = (Width % 2 == 0) ? Width / 2 : (Width - 1) / 2; // exclude center row if uneven
    for (unsigned short y = 0; y < Height; ++y)
    {
        for (unsigned short x = 0; x < halfWidth; ++x)
        {
            Color16b tmp = this->GetPixel(x, y);
            SetPixel(x, y, GetPixel((Width - x - 1), y));
            SetPixel((Width - x - 1), y, tmp);
        }
    }
}

void Sprite::FlipVertically()
{
    unsigned short halfHeight = (Height % 2 == 0) ? Height / 2 : (Height - 1) / 2; // exclude center colum if uneven.

    for (unsigned short x = 0; x < Width; ++x)
    {
        for (unsigned short y = 0; y < halfHeight; ++y)
        {
            Color16b tmp = this->GetPixel(x, y);
            SetPixel(x, y, GetPixel(x, (Height - y - 1)));
            SetPixel(x, (Height - y - 1), tmp);
        }
    }
}

