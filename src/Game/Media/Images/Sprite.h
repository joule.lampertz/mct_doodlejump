//
// Created by jlampertz on 23/06/2023.
//

#pragma once


#include "ColorRGB565.h"
#include "memory"

/**
 * the sprite class represents pixel sprites.
 * it allows for transparent pixel.
 */
class Sprite
{
public:
    Color16b *Bitmap = nullptr;
    unsigned short Width;
    unsigned short Height;
    Color16b TransparentColor;

    /**
     * creates a new sprite object, with an empty bitmap.
     * @param width width of the sprite.
     * @param height height of the sprite.
     * @param transparentColor pixel of this colour will be treated as transparent.
     */
    Sprite(short width, short height, Color16b transparentColor);

    /**
     * creates a new sprite object, from an existing bitmap.
     * @param bitmap bitmap for this sprite. (will be cloned over)
     * @param width width of the sprite.
     * @param height height of the sprite.
     * @param transparentColor pixel of this colour will be treated as transparent.
     */
    Sprite(Color16b *bitmap, unsigned short width, unsigned short height, Color16b transparentColor);

    /**
     * creates a new sprite object from an existing one.
     * the bitmap gets cloned over.
     * @param sprite sprite to copy.
     */
    Sprite(const Sprite &sprite);

    ~Sprite();

    /**
     * sets the colour of a pixel at a specific position. <br/>
     * x = 0, y = 0 is in the bottom left corner of the sprite.
     * @param posX horizontal position.
     * @param posY vertical position.
     * @param color colour for the pixel.
     */
    void SetPixel(unsigned short posX, unsigned short posY, Color16b color);

    /**
     * x = 0, y = 0 is in the bottom left corner of the sprite.
     * @param posX horizontal position.
     * @param posY vertical position.
     * @return the colour of the pixel at (x, y)
     */
    [[nodiscard]]
    Color16b GetPixel(unsigned short posX, unsigned short posY) const;

    /**
     * flips all pixel in the matrix at a horizontal axis in the center of the matrix.
     */
    void FlipHorizontally();

    /**
     * flips all pixel in the matrix at a vertical axis in the center of the matrix.
     */
    void FlipVertically();
};
