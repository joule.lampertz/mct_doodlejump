//
// Created by jlampertz on 23/06/2023.
//

#include "ColorRGB565.h"

Color16b ColorRGB565::Create16(unsigned char red, unsigned char green, unsigned char blue)
{
    red = red > MaxValRed ? MaxValRed : red;
    green = green > MaxValGreen ? MaxValGreen : green;
    blue = blue > MaxValBlue ? MaxValBlue : blue;

    return (red << 11) + (green << 5) + blue;
}

UG_COLOR ColorRGB565::Create32(unsigned char red, unsigned char green, unsigned char blue)
{
    return Convert16To32(Create16(red, green, blue));
}

UG_COLOR ColorRGB565::Convert16To32(Color16b color)
{
    return RGB565BitMask | color;
}
