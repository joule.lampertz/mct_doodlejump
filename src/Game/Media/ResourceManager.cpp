//
// Created by jlampertz on 07/06/2023.
//

#include <cstring>
#include "ResourceManager.h"
#include "../Game.h"


ResourceManager::ResourceManager(FatFs fileSys)
        : _fileSys(std::move(fileSys))
{
    posix_io::inst.register_fileio(_fileSys);
}

void ResourceManager::LoadSprites(const char **names, size_t length)
{
    // mount fs
    int mountResult = _fileSys.mount();
    if (mountResult != FatFs::FR_OK)
    {
        char msg[100];
        std::sprintf(msg, "unable to mount filesystem. fatfs code: %d.", mountResult);
        Game::GetInstance()->Loggy.Log(msg, ERROR);
    } else
    {
        char msg[100];
        std::sprintf(msg, "filesystem mounted.");
        Game::GetInstance()->Loggy.Log(msg, SUCCESS);
    }
    yahal_assert(mountResult == FatFs::FR_OK);

    // open files for reading
    for (unsigned int i = 0; i < length; ++i)
    {
        FatFs::FILE f{};
        // open meta information file
        {
            std::string filename = std::string{names[i]} + ".mta";
            int openResult = _fileSys.open(&f, filename.c_str(), FA_READ);
            if (openResult != FatFs::FR_OK)
            {
                char msg[150];
                std::sprintf(msg, "unable to read resource file. path: \"%s\". fatfs code: %d.", filename.c_str(),
                             openResult);
                Game::GetInstance()->Loggy.Log(msg, WARNING);
            }
        }

        unsigned short nBytesRead = 0;

        // read meta data
        unsigned short meta[3]; // 0: width, 1: height, 2: transparent color
        _fileSys.read(&f, meta, 3 * 16, &nBytesRead);
        _fileSys.close(&f);


        // open _sprite file
        {
            std::string filename = std::string{names[i]} + ".bm";
            int openResult = _fileSys.open(&f, filename.c_str(), FA_READ);
            if (openResult != FatFs::FR_OK)
            {
                char msg[150];
                std::sprintf(msg, "unable to read resource file. path: \"%s\". fatfs code: %d.", filename.c_str(),
                             openResult);
                Game::GetInstance()->Loggy.Log(msg, WARNING);
            }
        }

        // read _sprite
        Color16b *bitmap = new Color16b[meta[0] * meta[1]];
        _fileSys.read(&f, bitmap, meta[0] * meta[1] * sizeof(Color16b), &nBytesRead);
        _fileSys.close(&f);

        // create _sprite
        Sprite *sprite = new Sprite(bitmap, meta[0], meta[1], meta[2]);
        Sprites[names[i]] = sprite;
    }

    // umount fs
    int umountResult = _fileSys.umount();
    if (umountResult != FatFs::FR_OK)
    {
        char msg[100];
        std::sprintf(msg, "unable to umount filesystem. fatfS code: %d.", umountResult);
        Game::GetInstance()->Loggy.Log(msg, ERROR);
    }
}

ResourceManager::~ResourceManager()
{
    for (auto kv: Sprites)
    {
        delete kv.second;
    }
    Sprites.clear();
}
