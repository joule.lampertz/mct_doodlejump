#pragma once

#include <string>
#include "ff.h"
#include "Sound/Sound.h"
#include "Images/Sprite.h"
#include "map"
#include "posix_io.h"
#include <utility>


class ResourceManager
{
private:
    FatFs _fileSys;

public:
    /** map of loaded sprites. */
    std::map<const char *, Sprite *> Sprites{};

    /**
     * create a new instance of the resource manager class.
     * @param fileSys filesystem to load the resources from.
     */
    explicit ResourceManager(FatFs fileSys);

    ~ResourceManager();

    /**
     * loads a list of sprites. <br/>
     * there should be two corresponding files in the filesystem for each name in the list.
     * one with the extension .bm containing the bitmap of the sprite
     * and one with the extension .mta containing meta data about the sprite.
     * @param names array containing the names of the sprites.
     * @param length length of the name array.
     */
    void LoadSprites(const char **names, size_t length);
};
