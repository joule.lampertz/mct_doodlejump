//
// Created by jlampertz on 20/07/2023.
//

#include "Level.h"
#include "GameObjects/Camera/Camera.h"
#include "../Game.h"
#include "../../Util/Math/Random.h"
#include "GameObjects/GameOverScreen.h"
#include "GameObjects/Platforms/DefaultPlatform.h"

const float Level::LOWER_BOUND_OFFSET = -3;

Level::~Level()
{
    Clear();
    delete PlatformFactories[0];
    delete PlatformFactories[1];
}


void Level::Clear()
{
    for (IGameObject *obj: Objects)
    {
        delete obj;
    }
    Objects.clear();
}


void Level::InitLevel()
{
    GameOver = false;

    // init camera object
    Cam = new Camera{Game::DISPLAY_WIDTH, Game::DISPLAY_HEIGHT};
    Camera *concreteCam = (Camera *) Cam; // Camera is both IGameObject and ICamera
    Objects.push_back(concreteCam); // camera at Objects[0]
    concreteCam->CenterOn(StartPosition, true);

    // spawn player above start position, slightly moved to the left, to be centered
    Player = new Jumper{StartPosition + Vector2D{-12, 10}};
    Objects.push_back(Player); // player at Objects[1]
    concreteCam->Follows = Player; // make camera follow the player

    // spawn in platform for player to start on
    Platform *base = new DefaultPlatform{StartPosition + Vector2D{-(float) Platform::WIDTH / 2, 0}};
    Objects.push_back(base);
    LatestPlatformY = StartPosition.Y;

    // init all game objects
    for (IGameObject *obj: Objects)
    {
        obj->Init();
    }

    // generate first batch of random platforms
    CreateNewPlatforms();
}

void Level::UpdateGameObjects()
{
    for (IGameObject *obj: Objects)
    {
        obj->Update();
    }
}

void Level::CheckCollision()
{
    for (unsigned int i = 0; i < Objects.size(); i++)
    {
        if (!Objects[i]->GetHitbox().Enabled) continue;

        for (unsigned int j = i + 1; j < Objects.size(); j++)
        {
            if (!Hitbox::Overlaps(*Objects[i], *Objects[j])) continue;

            Objects[i]->CollisionHandler(*Objects[j]);
            Objects[j]->CollisionHandler(*Objects[i]);

            // notify cam
            Objects[0]->CollisionHandler(*Objects[i]);
            Objects[0]->CollisionHandler(*Objects[j]);
        }
    }
}

void Level::CheckLoss()
{
    if (Player->Alive && Player->GetPosition().Y < Cam->GetPosition().Y + LOWER_BOUND_OFFSET)
        OverGame();
}

void Level::GeneratePlatforms(std::vector<IGameObject *> &out, float y, int minX, int maxX)
{
    unsigned short amount = (unsigned short) Random::GenerateWeightedInt(PlatformAmountWeights, 3);
    int length = std::abs(maxX - minX) / amount;

    for (unsigned short i = 0; i < amount; i++)
    {
        int min = minX + (i * length);
        int x = Random::GenerateInt(min, min + length - Platform::WIDTH);
        unsigned short type = (unsigned short) Random::GenerateWeightedInt(PlatformTypeWeights, 2);

        Platform *platform = PlatformFactories[type]->Create({(float) x, y});
        platform->Init();
        out.push_back(platform);
    }
}

void Level::DeleteOldPlatforms()
{
    auto iter = Objects.begin();
    while (iter != Objects.end())
    {
        if (Cam->GetPosition().Y - (*iter)->GetPosition().Y > PlatformDeleteDistance)
        {
            delete *iter;
            iter = Objects.erase(iter);
        } else
        {
            ++iter;
        }
    }
}

void Level::CreateNewPlatforms()
{
    Camera *camera = (Camera *) Cam;
    while (LatestPlatformY + 20 < (camera->GetPosition().Y + (float) camera->Height))
    {
        LatestPlatformY += PlatformSpacing;
        GeneratePlatforms(Objects, LatestPlatformY, -64, 64);
    }
}

void Level::OverGame()
{
    GameOver = true;
    Player->Kill();
    ((Camera *) Objects[0])->Mode = Camera::FOLLOW;

    GameOverScreen *gameOver = new GameOverScreen{};
    gameOver->Init();
    Objects.push_back(gameOver);
}
