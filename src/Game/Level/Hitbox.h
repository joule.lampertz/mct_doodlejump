//
// Created by jlampertz on 18/07/2023.
//

#pragma once

#include "../../Util/Math/Vector2D.h"
#include "GameObjects/IGameObject.h"

class IGameObject;

/**
 * the hitbox class represents a 2d rectangle with a position and
 * contains functionality to check whether two hitboxes overlap
 */
class Hitbox
{

public:
    /** a deactivated hitbox with size (0, 0) at position (0, 0). */
    static const Hitbox ZERO;
    /** whether the position of the hitbox is relative to the position of a parent object */
    bool Relative = true;
    /** whether overlaps are registered for this hitbox. */
    bool Enabled = true;
    /** height and width of the hitbox */
    Vector2D Size{};
    /** position of the lower left corner of the hitbox */
    Vector2D Position{};

    /**
     * creates a new hitbox object.
     * @param size height and width of the hitbox.
     * @param relative whether the hitbox position is relative to the position of the parent object.
     * @param enabled whether overlaps are registered for this hitbox.
     */
    Hitbox(Vector2D size, bool relative = true, bool enabled = true);

    /**
     * creates a new hitbox object.
     * @param position the position of the hitbox
     * @param size height and width of the hitbox.
     * @param relative whether the hitbox position is relative to the position of the parent object.
     * @param enabled whether overlaps are registered for this hitbox.
     */
    Hitbox(Vector2D position, Vector2D size, bool relative = true, bool enabled = true);

    /**
     * @param other the other hitbox.
     * @return whether this hitbox overlaps with the other hitbox.
     */
    [[nodiscard]]
    bool Overlaps(const Hitbox &other) const;

    /**
     * @param hitbox1 one hitbox
     * @param hitbox2 another hitbox
     * @return whether the two hitboxes overlap.
     */
    static bool Overlaps(const Hitbox &hitbox1, const Hitbox &hitbox2);

    /**
     * @param obj1 one game object.
     * @param obj2 another game object.
     * @return whether the hitboxes of the two game objects overlap.
     */
    static bool Overlaps(const IGameObject &obj1, const IGameObject &obj2);
};
