//
// Created by jlampertz on 18/07/2023.
//

#include "Hitbox.h"
#include "GameObjects/IGameObject.h"

const Hitbox Hitbox::ZERO = Hitbox{{0, 0}, false, false};


Hitbox::Hitbox(Vector2D size, bool relative, bool enabled)
        : Relative(relative), Enabled(enabled), Size(size)
{

}

Hitbox::Hitbox(Vector2D position, Vector2D size, bool relative, bool enabled)
        : Relative(relative), Enabled(enabled), Size(size), Position(position)
{

}

bool Hitbox::Overlaps(const Hitbox &other) const
{
    return Overlaps(*this, other);
}

bool Hitbox::Overlaps(const Hitbox &hitbox1, const Hitbox &hitbox2)
{
    return
            hitbox1.Enabled && hitbox2.Enabled &&
            hitbox1.Position.X < hitbox2.Position.X + hitbox2.Size.X &&
            hitbox1.Position.X + hitbox1.Size.X > hitbox2.Position.X &&
            hitbox1.Position.Y < hitbox2.Position.Y + hitbox2.Size.Y &&
            hitbox1.Position.Y + hitbox1.Size.Y > hitbox2.Position.Y;
}

bool Hitbox::Overlaps(const IGameObject &obj1, const IGameObject &obj2)
{
    const Hitbox &hitbox1 = obj1.GetHitbox();
    const Hitbox &hitbox2 = obj2.GetHitbox();

    if (!hitbox1.Enabled || !hitbox2.Enabled) return false;

    Vector2D pos1 = hitbox1.Relative ? obj1.GetPosition() + hitbox1.Position : hitbox1.Position;
    Vector2D pos2 = hitbox2.Relative ? obj2.GetPosition() + hitbox2.Position : hitbox2.Position;
    return
            pos1.X < pos2.X + hitbox2.Size.X &&
            pos1.X + hitbox1.Size.X > pos2.X &&
            pos1.Y < pos2.Y + hitbox2.Size.Y &&
            pos1.Y + hitbox1.Size.Y > pos2.Y;
}
