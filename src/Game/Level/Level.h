//
// Created by jlampertz on 20/07/2023.
//

#pragma once

#include <vector>
#include "../../Util/Math/Vector2D.h"
#include "GameObjects/IGameObject.h"
#include "GameObjects/Jumper.h"
#include "GameObjects/Platforms/Platform.h"
#include "GameObjects/Platforms/IPlatformFactory.h"
#include "GameObjects/Platforms/DefaultPlatformFactory.h"
#include "GameObjects/Platforms/DisappearingPlatformFactory.h"

/**
 * the level class contains game parameter, all of the game objects and methods to update them.
 */
class Level
{
protected:
    /** offset to camera position below which the player gets killed */
    static const float LOWER_BOUND_OFFSET;

    /** when a platform gets this far below the camera it should get deleted */
    const float PlatformDeleteDistance = 100;
    /** vertical distance between platforms */
    const float PlatformSpacing = 35;

    /** vertical position of the latest spawned platform */
    float LatestPlatformY = StartPosition.Y;

    /** platform amounts and their respective weights for platform generation */
    const std::pair<int, float> PlatformAmountWeights[3] =
            {
                    std::pair{1, 6},
                    std::pair{2, 2},
                    std::pair{3, 1},
            };

    /** platform factories used in platform generation */
    const IPlatformFactory *PlatformFactories[2] =
            {
                    new DefaultPlatformFactory{},
                    new DisappearingPlatformFactory{},
            };

    /** platform factory indexes and their respective weights in platform generation. */
    const std::pair<int, float> PlatformTypeWeights[3] =
            {
                    std::pair{0, 7},
                    std::pair{1, 1},
            };


    /**
     * generates one random level of platforms to a game object vector.
     * @param out where the platforms get generated to.
     * @param y height of the platforms.
     * @param minX minimal position of a left platform corner.
     * @param maxX maximal position of a right platform corner.
     */
    void GeneratePlatforms(std::vector<IGameObject *> &out, float y, int minX, int maxX);

    /**
     * makes the game end.
     */
    void OverGame();

public:
    /**
     * whether everything that leaves the display on one side should reappear on the other side
     * (not implemented yet)
     */
    const bool Looping = false;

    /** where to generate the first platform */
    const Vector2D StartPosition{0, 0};

    /** whether the game has ended */
    bool GameOver = false;

    /** pointer to the camera */
    ICamera *Cam = nullptr; // is also a game object but can't be declared as one here or there would be an include error
    /** pointer to the player character */
    Jumper *Player = nullptr;
    /** vector of all game objects in the level */
    std::vector<IGameObject *> Objects{};

    Level() = default;

    ~Level();

    /** sets up everything for tis level to run. */
    void InitLevel();

    /** updates all game objects in the level. */
    void UpdateGameObjects();

    /** checks for collisions between all game objects and notifies their handler. */
    void CheckCollision();

    /** checks whether the loss criteria is met and ends the game if so.  */
    void CheckLoss();

    /** deletes platforms that are too far below the camera position */
    void DeleteOldPlatforms();

    /** fills an area, up to a specific distance above the camera, with randomly generate platforms */
    void CreateNewPlatforms();

    /** deletes all game objects (including player and camera) and the platform factories. */
    void Clear();
};
