//
// Created by jlampertz on 17/07/2023.
//

#pragma once

#include "../../DisplayMatrix.h"
#include "../../../Util/Math/Vector2D.h"
#include "../Hitbox.h"
#include "Camera/ICamera.h"

class Hitbox;

class ICamera;

/**
 * the IGameObject interfece represents an object in the game, with
 * position, hitbox, collision management, drawing to an ICamera and movement.
 */
class IGameObject
{
public:
    /** types of game objects */
    enum Type : short
    {
        UNDEFINED = 0,
        WEAPON,
        PLAYER,
        PLATFORM,
        ENEMY,
    };

    virtual ~IGameObject() = default;

    /** returns this game objects type */
    [[nodiscard]]
    inline virtual Type GetType() const { return UNDEFINED; };

    /** initialises the game object */
    virtual void Init() = 0;

    /** updates the game object according to its defined behaviour */
    virtual void Update() = 0;

    /**
     * lets a camera capture this game object.
     * @param camera
     */
    virtual void Draw(ICamera &camera) const = 0;

    /**
     * manages what happens when this game object collides with another game object.
     * @param obj the game object it collided with.
     */
    virtual void CollisionHandler(const IGameObject &obj) = 0;

    /**
     * @return the hitbox of this game object.
     */
    [[nodiscard]]
    virtual const Hitbox &GetHitbox() const = 0;

    /**
     * @return a reference to the position of this game object.
     */
    [[nodiscard]]
    virtual Vector2D &GetPosition() = 0;

    /**
     * @return the position of this game object.
     */
    [[nodiscard]]
    virtual Vector2D GetPosition() const = 0;

    /**
     * sets a new position for this game object.
     * @param position the new position of this game object.
     */
    virtual void SetPosition(Vector2D position) = 0;

    /**
     * @return whether this game object should be captured by cameras or not.
     */
    [[nodiscard]]
    virtual bool IsVisible() const = 0;

    /**
     * moves this game object a given amount towards a given position.
     * @param position position to move toward.
     * @param step how far the game object should move. (less if goal is closer)
     */
    void MoveTowards(Vector2D position, float step);
};
