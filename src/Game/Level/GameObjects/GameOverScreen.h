//
// Created by jlampertz on 24/07/2023.
//

#pragma once


#include "IGameObject.h"

/** draws animates "GAME OVER" and "press A" on screen. */
class GameOverScreen : public IGameObject
{
private:
    Vector2D _position = {0, 0};
    /** times animation */
    unsigned short _blinkTimer = 0;

    /** loads the required sprites from the games resource manager. */
    void _loadSprites();

protected:
    /** how many game updates the button takes to change its state. */
    const unsigned short ButtonDelaySpeed = 20;
    /** current state of the button, */
    unsigned char ButtonState = 0;
    /** pointer to the "GAME OVER" text sprite. */
    Sprite *GameOverSprite = nullptr;
    /** pointer to the "press" text sprite. */
    Sprite *PressSprite = nullptr;
    /** list with the two button A sprites. */
    Sprite *ButtonSprites[2]{};

    /** where to display the "GAME OVER" text. */
    Vector2D GameOverPosition{18, 40};

    /** where to display the "press" text. */
    Vector2D PressPosition{40, 25};

    /** where to display the button A sprite. */
    Vector2D ButtonPosition{75, 24};
public:

    /** creates a new GameOverScreen object. */
    GameOverScreen() = default;

    void Init() override;

    void Update() override;

    void Draw(ICamera &camera) const override;

    inline void CollisionHandler(const IGameObject &) override {};

    [[nodiscard]]
    inline const Hitbox &GetHitbox() const override { return Hitbox::ZERO; };

    [[nodiscard]]
    inline Vector2D &GetPosition() override { return _position; };

    [[nodiscard]]
    inline Vector2D GetPosition() const override { return _position; }

    inline void SetPosition(Vector2D position) override { _position = position; };

    [[nodiscard]]
    inline bool IsVisible() const override { return true; };
};
