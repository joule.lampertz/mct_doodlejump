#include "Jumper.h"
#include "../../Game.h"
#include "Platforms/Platform.h"
#include <cmath>


Jumper::Jumper(Vector2D position)
        : Position(position)
{

}

void Jumper::Init()
{
    loadSprites();
}

void Jumper::loadSprites()
{
    ResourceManager &resources = Game::GetInstance()->Resources;

    Sprites[UP_LEFT] = resources.Sprites["JumpL"];
    Sprites[UP_RIGHT] = resources.Sprites["JumpR"];
    Sprites[UP_CENTER] = resources.Sprites["JumpC"];
    Sprites[DOWN_LEFT] = resources.Sprites["JumpDL"];
    Sprites[DOWN_RIGHT] = resources.Sprites["JumpDR"];
}

void Jumper::Update()
{
    SetPosition(GetPosition() + Velocity);
    Velocity += GRAVITY;

    if ((int) GetPosition().Y > (GetTopHeight()))
    {
        TopHeight = (int) GetPosition().Y;
        char msg[100];
        std::sprintf(msg, "TOP HEIGHT: %d", GetTopHeight());
        Game::GetInstance()->Loggy.Log(msg);
    }

    updateState();
}

void Jumper::Draw(ICamera &camera) const
{
    camera.GetDisplayMatrix().DrawSprite(
            *Sprites[CurrentState],
            (int) (GetPosition().X - camera.GetPosition().X),
            (int) (GetPosition().Y - camera.GetPosition().Y),
            true);
}

void Jumper::CollisionHandler(const IGameObject &)
{

}

void Jumper::Kill()
{
    Alive = false;
    Box.Enabled = false;
}

void Jumper::updateState()
{
    if (Velocity.X < 0.5)
    {
        CurrentState = UP_LEFT;
    } else if (Velocity.X > 0.5)
    {
        CurrentState = UP_RIGHT;
    }

    // turns jumper upside down if dead.
    if (!Alive)
    {
        switch (CurrentState)
        {
            case UP_CENTER:
            case UP_LEFT:
                CurrentState = DOWN_LEFT;
                break;
            case UP_RIGHT:
                CurrentState = DOWN_RIGHT;
                break;
            default:
                break;
        }
    }
}

