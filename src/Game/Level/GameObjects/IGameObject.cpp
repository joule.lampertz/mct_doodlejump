//
// Created by jlampertz on 17/07/2023.
//

#include "IGameObject.h"

void IGameObject::MoveTowards(Vector2D position, float step)
{
    Vector2D delta = position - this->GetPosition();
    float dist = delta.GetLength();

    if (dist <= step)
    {
        this->SetPosition(position);
        return;
    }

    this->GetPosition() += delta.Normalize() * step;
}
