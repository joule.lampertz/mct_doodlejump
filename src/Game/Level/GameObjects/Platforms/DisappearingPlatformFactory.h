//
// Created by jlampertz on 25/07/2023.
//

#pragma once


#include "IPlatformFactory.h"

/** factory for creating DisappearingPlatform objects */
class DisappearingPlatformFactory : public IPlatformFactory
{
public:
    /**
     * @param position position of the new platform.
     * @return a new DisappearingPlatform object.
     */
    [[nodiscard]]
    Platform *Create(Vector2D position) const override;
};
