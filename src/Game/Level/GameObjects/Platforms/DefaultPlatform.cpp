//
// Created by jlampertz on 25/07/2023.
//

#include "DefaultPlatform.h"
#include "../../../Media/ResourceManager.h"
#include "../../../Game.h"

void DefaultPlatform::Init()
{
    ResourceManager &resources = Game::GetInstance()->Resources;
    _sprite = resources.Sprites["Plat"];
}

void DefaultPlatform::Draw(ICamera &camera) const
{
    camera.GetDisplayMatrix().DrawSprite(
            *_sprite,
            (int) (GetPosition().X - camera.GetPosition().X),
            (int) (GetPosition().Y - camera.GetPosition().Y),
            true);
}

DefaultPlatform::DefaultPlatform(Vector2D position)
{
    Position = position;
    Box = Hitbox{{(float) WIDTH, (float) HEIGHT}, true, true};
}

void DefaultPlatform::CollisionHandler(const IGameObject &obj)
{
    if (obj.GetType() == PLAYER)
    {
        Jumper *player = (Jumper *) &obj;
        if (player->Velocity.Y <= 0) // player falls onto platform (not just passing through)
        {
            player->Velocity = BOOST_VEL;
        }
    }
}
