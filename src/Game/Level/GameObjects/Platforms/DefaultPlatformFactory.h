//
// Created by jlampertz on 25/07/2023.
//

#pragma once


#include "IPlatformFactory.h"

/** factory for creating DefaultPlatform objects */
class DefaultPlatformFactory : public IPlatformFactory
{
public:
    /**
     * @param position position of the new platform.
     * @return a new DefaultPlatform object.
     */
    [[nodiscard]]
    Platform *Create(Vector2D position) const override;
};
