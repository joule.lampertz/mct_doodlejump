//
// Created by jlampertz on 25/07/2023.
//

#include "DisappearingPlatformFactory.h"
#include "DisappearingPlatform.h"

Platform *DisappearingPlatformFactory::Create(Vector2D position) const
{
    return new DisappearingPlatform{position};
}
