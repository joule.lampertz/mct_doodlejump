//
// Created by jlampertz on 19/07/2023.
//

#pragma once

#include "../IGameObject.h"

/** the abstract Platform class represents a platform for the player to jump on */
class Platform : public IGameObject
{
public:
    /** width of the platform */
    static const unsigned short WIDTH;
    /** height of the platform */
    static const unsigned short HEIGHT;
protected:
    /** position of the platform */
    Vector2D Position{};
    /** hitbox of the platform */
    Hitbox Box{Hitbox::ZERO};
public:
    [[nodiscard]]
    inline Type GetType() const override { return PLATFORM; }

    [[nodiscard]]
    inline const Hitbox &GetHitbox() const override { return Box; }

    [[nodiscard]]
    inline Vector2D &GetPosition() override { return Position; }

    [[nodiscard]]
    inline Vector2D GetPosition() const override { return Position; }

    inline void SetPosition(Vector2D position) override { Position = position; }

    [[nodiscard]]
    inline bool IsVisible() const override { return true; }
};
