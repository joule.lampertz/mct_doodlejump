//
// Created by jlampertz on 25/07/2023.
//

#pragma once


#include "Platform.h"

/** the DisappearingPlatform class represents a platform that disappears once jumped on. */
class DisappearingPlatform : public Platform
{
private:
    /** pointer to the sprite the platform uses when not yet jumped on. */
    Sprite *_solidSprite = nullptr;
    /** pointer to the sprite the platform uses once jumped on. */
    Sprite *_outlineSprite = nullptr;
    /** pointer to the current sprite (either solid or outlined). */
    Sprite *_currentSprite = nullptr;

    /** changes platform behaviour and graphic representation to non-solid. */
    void disappear();

protected:
    /** velocity player gains by jumping on this platform. */
    const Vector2D BOOST_VEL{0, 4};
public:

    /**
     * creates a new DisappearingPlatform object.
     * @param position position of the new platform (lower left corner).
     */
    explicit DisappearingPlatform(Vector2D position);

    void Init() override;

    inline void Update() override {};

    void Draw(ICamera &camera) const override;

    inline void CollisionHandler(const IGameObject &obj) override;
};
