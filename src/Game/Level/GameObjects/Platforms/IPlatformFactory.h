//
// Created by jlampertz on 17/07/2023.
//

#pragma once

#include "Platform.h"
#include "../../../../Util/Math/Vector2D.h"

/** interface for creating platforms. */
class IPlatformFactory
{
public:

    virtual ~IPlatformFactory() = default;

    /**
     * @param position position of the new platform  (lower left corner).
     * @return a new platform.
     */
    [[nodiscard]]
    virtual Platform *Create(Vector2D position) const = 0;
};
