//
// Created by jlampertz on 25/07/2023.
//

#pragma once


#include "Platform.h"

/** represents a static platform, for the player to jump on. */
class DefaultPlatform : public Platform
{
private:
    /** pointer to the platforms sprite. */
    Sprite *_sprite = nullptr;
protected:
    /** velocity player gains by jumping on this platform. */
    const Vector2D BOOST_VEL{0, 4};
public:
    /**
     * creates a new DefaultPlatform object.
     * @param position position of the new platform (lower left corner).
     */
    explicit DefaultPlatform(Vector2D position);

    void Init() override;

    inline void Update() override {};

    void Draw(ICamera &camera) const override;

    inline void CollisionHandler(const IGameObject &obj) override;
};
