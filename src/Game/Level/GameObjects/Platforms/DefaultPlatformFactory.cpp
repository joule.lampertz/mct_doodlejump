//
// Created by jlampertz on 25/07/2023.
//

#include "DefaultPlatformFactory.h"
#include "DefaultPlatform.h"

Platform *DefaultPlatformFactory::Create(Vector2D position) const
{
    return new DefaultPlatform{position};
}
