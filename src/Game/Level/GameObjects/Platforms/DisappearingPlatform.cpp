//
// Created by jlampertz on 25/07/2023.
//

#include "DisappearingPlatform.h"
#include "../../../Media/ResourceManager.h"
#include "../../../Game.h"

void DisappearingPlatform::Init()
{
    ResourceManager &resources = Game::GetInstance()->Resources;
    _solidSprite = resources.Sprites["PlatApp"];
    _outlineSprite = resources.Sprites["PlatDis"];
    _currentSprite = _solidSprite;
}

void DisappearingPlatform::Draw(ICamera &camera) const
{
    camera.GetDisplayMatrix().DrawSprite(
            *_currentSprite,
            (int) (GetPosition().X - camera.GetPosition().X),
            (int) (GetPosition().Y - camera.GetPosition().Y),
            true);
}

void DisappearingPlatform::CollisionHandler(const IGameObject &obj)
{
    if (obj.GetType() == PLAYER)
    {
        Jumper *player = (Jumper *) &obj;
        if (player->Velocity.Y <= 0) // player falls onto platform (not just passing through)
        {
            player->Velocity = BOOST_VEL;
            disappear();
        }
    }
}

void DisappearingPlatform::disappear()
{
    _currentSprite = _outlineSprite;
    Box.Enabled = false;
}

DisappearingPlatform::DisappearingPlatform(Vector2D position)
{
    Position = position;
    Box = Hitbox{{(float) WIDTH, (float) HEIGHT}, true, true};
}
