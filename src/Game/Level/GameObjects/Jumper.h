//
// Created by jlampertz on 17/07/2023.
//

#pragma once


#include "IGameObject.h"

/**
 * the Jumper class represents a little jumper bug, controlled by the player.
 */
class Jumper : public IGameObject
{
protected:
    enum JumperState : short
    {
        UP_LEFT,
        UP_RIGHT,
        UP_CENTER,
        DOWN_LEFT,
        DOWN_RIGHT,
    };

    /** current state of the jumper. */
    short CurrentState{};
    /** array of all sprites the jumper might be displayed as. */
    Sprite *Sprites[5]{};
    /** hitbox of the jumper (doesn't match sprite). */
    Hitbox Box{Vector2D{23, 4}};
    /** position of the jumper */
    Vector2D Position{};
    /** highest height the jumper reached during this run. */
    int TopHeight = 0;

    /** gets the required sprites from the games resource manager */
    void loadSprites();

    /** updates the state of the jumper */
    void updateState();

public:
    /** gravity acceleration */
    const Vector2D GRAVITY{0, -0.2f};
    /** to be multiplied with the user input for horizontal velocity */
    const float HORIZONTAL_MULTIPLIER = 30.f;

    /** whether the jumper is still alive */
    bool Alive{true};
    /** current velocity of the jumper */
    Vector2D Velocity{};

    /** creates a new jumper object */
    Jumper() = default;

    /**
     * creates a new jumper object
     * @param position position of the jumper (lower left corner).
     */
    explicit Jumper(Vector2D position);


    void Init() override;

    void Update() override;

    void Draw(ICamera &camera) const override;

    void CollisionHandler(const IGameObject &obj) override;

    /** switches jumper to its game over behaviour */
    void Kill();


// getter and setter

    [[nodiscard]]
    inline Type GetType() const override { return PLAYER; }

    [[nodiscard]]
    inline const Hitbox &GetHitbox() const override { return Box; }

    [[nodiscard]]
    inline Vector2D &GetPosition() override { return Position; }

    [[nodiscard]]
    inline Vector2D GetPosition() const override { return Position; }

    inline void SetPosition(Vector2D position) override { Position = position; }

    [[nodiscard]]
    inline bool IsVisible() const override { return true; }

    /**
     * @return highest height the jumper reached during this run.
     */
    [[nodiscard]]
    inline int GetTopHeight() const { return TopHeight; }
};
