//
// Created by jlampertz on 20/07/2023.
//

#include "Camera.h"
#include "../../Level.h"
#include "../../../Game.h"

const float Camera::CLIMB_SCROLL_SPEED = 1.2f;
const float Camera::FOLLOW_SCROLL_SPEED = 10.f;
const float Camera::CLIM_BUFFER = 10.f;

Camera::Camera(unsigned short viewWidth, unsigned short viewHeight)
{
    Width = viewWidth;
    Height = viewHeight;
}

void Camera::CenterOn(Vector2D position, bool resetGoal)
{
    Vector2D &pos = GetPosition();
    pos = position;
    pos.X -= (float) Width / 2.f;
    pos.Y -= (float) Height / 2.f;

    if (resetGoal) ResetGoal();
}

void Camera::ResetGoal()
{
    SetGoalPosition(GetPosition());
}

void Camera::Init()
{
    _displayMatrix = &Game::GetInstance()->DM;
}

void Camera::Update()
{
    switch (Mode)
    {
        case STATIC:
            return;
        case CLIMB:
            MoveTowards(_goalPosition, CLIMB_SCROLL_SPEED);
            break;
        case FOLLOW:
            float x = GetPosition().X;
            CenterGoalOn(Follows->GetPosition());
            GetGoalPosition().X = x;
            MoveTowards(_goalPosition, FOLLOW_SCROLL_SPEED);
            break;
    }

}

void Camera::CaptureScene(std::vector<IGameObject *> objectsInScene)
{
    _displayMatrix->Fill(ColorRGB565::SKY_BLUE);
    for (unsigned int i = objectsInScene.size(); i-- > 0;)
    {
        IGameObject *obj = objectsInScene[i];
        if (!obj->IsVisible()) continue;
        obj->Draw(*this);
    }
}

void Camera::CollisionHandler(const IGameObject &obj)
{
    if (Mode != CLIMB) return;

    if (obj.GetType() == IGameObject::Type::PLATFORM)
    {
        if (obj.GetPosition().Y > GetPosition().Y)
        {
            GetGoalPosition().Y = obj.GetPosition().Y - CLIM_BUFFER;
        }
    }
}

void Camera::CenterGoalOn(Vector2D &position)
{
    Vector2D &goal = GetGoalPosition();
    goal = position;
    goal.X -= (float) Width / 2.f;
    goal.Y -= (float) Height / 2.f;
}
