//
// Created by jlampertz on 20/07/2023.
//

#pragma once

#include "../IGameObject.h"
#include "ICamera.h"
#include "../../../../Util/Math/Vector2D.h"
#include "../../Hitbox.h"

/** the Camera class is an IGameObject that represents a camera (implements ICamera). */
class Camera : public IGameObject, public ICamera
{
private:
    static const Hitbox HITBOX;
    /** when calculating the new height in climb mode, this will be added to the current target platforms y position. */
    static const float CLIM_BUFFER;
    /** camera speed in climb mode. */
    static const float CLIMB_SCROLL_SPEED;
    /** camera speed in follow mode */
    static const float FOLLOW_SCROLL_SPEED;
    /** DisplayMatrix the camera draws to */
    DisplayMatrix *_displayMatrix = nullptr;

    /** camera position */
    Vector2D _position{};
    /** the position the camera is moving towards */
    Vector2D _goalPosition{};
public:
    enum CameraMode
    {
        /** camera goes to the height of the platform that was last jumped on. */
        CLIMB,
        /** camera follows a specific game object. */
        FOLLOW,
        /** the camera doesn't move (automatically). */
        STATIC,
    };

    /** current camera mode (decides camera behaviour). */
    CameraMode Mode{CLIMB};
    /** camera capture width. */
    unsigned short Width;
    /** camera capture height. */
    unsigned short Height;
    /** the object the camera is currently following (if in CameraMode::FOLLOW). */
    IGameObject *Follows = nullptr;

    /**
     * creates a new Camera object.
     * @param viewWidth
     * @param viewHeight
     */
    Camera(unsigned short viewWidth, unsigned short viewHeight);

    void Init() override;

    void Update() override;

    inline void Draw(ICamera &) const override {};

    void CollisionHandler(const IGameObject &obj) override;


    /**
     * sets the camera position so that a given position is in the center of the view.
     * @param position position to center on.
     * @param resetGoal whether the goal position should be reset.
     */
    void CenterOn(Vector2D position, bool resetGoal = false);

    /** sets the goal position to the current camera position. */
    void ResetGoal();

    void CaptureScene(std::vector<IGameObject *> objectsInScene) override;

    [[nodiscard]]
    inline DisplayMatrix &GetDisplayMatrix() override { return *_displayMatrix; }

    [[nodiscard]]
    inline const Hitbox &GetHitbox() const override { return Hitbox::ZERO; }

    [[nodiscard]]
    inline Vector2D &GetPosition() override { return _position; }

    [[nodiscard]]
    inline Vector2D GetPosition() const override { return _position; }

    /**
     * sets the display matrix the camera is going to draw to.
     * @param displayMatrix output matrix.
     */
    inline void SetDisplayMatrix(DisplayMatrix &displayMatrix) { _displayMatrix = &displayMatrix; }

    inline void SetPosition(Vector2D position) override { _position = position; }

    [[nodiscard]]
    inline bool IsVisible() const override { return false; }

    /**
     * @return a reference to the position the camera is moving towards.
     */
    [[nodiscard]]
    inline Vector2D &GetGoalPosition() { return _goalPosition; }

    /**
     * @return the position the camera is moving towards.
     */
    [[nodiscard]]
    inline Vector2D GetGoalPosition() const { return _goalPosition; }

    /**
     * sets the position the camera should move towards.
     * @param position
     */
    inline void SetGoalPosition(Vector2D position) { _goalPosition = position; }

    /**
     * centers the cameras goal state on a specific position.
     * @param position position that will be in the center when the camera reaches its goal.
     */
    void CenterGoalOn(Vector2D &position);
};
