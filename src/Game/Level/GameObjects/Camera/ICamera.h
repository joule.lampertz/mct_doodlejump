//
// Created by jlampertz on 21/07/2023.
//

#pragma once


#include <vector>
#include "../../../DisplayMatrix.h"
#include "../../../../Util/Math/Vector2D.h"
#include "../IGameObject.h"

class IGameObject;

/**
 * the ICamera interface can be implemented represents a camera
 */
class ICamera
{
public:
    virtual ~ICamera() = default;

    /**
     * draw game objects from a game object vector to a DisplayMatrix object.
     * @param objectsInScene
     */
    virtual void CaptureScene(std::vector<IGameObject *> objectsInScene) = 0;

    /**
     * @return a reference to the DisplayMatrix object the camera draws to.
     */
    [[nodiscard]]
    virtual inline DisplayMatrix &GetDisplayMatrix() = 0;

    /**
     * @return the position of the camera (bottom left corner).
     */
    [[nodiscard]]
    virtual inline Vector2D GetPosition() const = 0;
};
