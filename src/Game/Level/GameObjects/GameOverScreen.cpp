//
// Created by jlampertz on 24/07/2023.
//

#include "GameOverScreen.h"
#include "../../Media/ResourceManager.h"
#include "../../Game.h"

void GameOverScreen::Init()
{
    _loadSprites();
}

void GameOverScreen::Update()
{
    _blinkTimer++;
    if (_blinkTimer > ButtonDelaySpeed)
    {
        _blinkTimer = 0;
        ButtonState = (ButtonState + 1) % 2;
    }
}

void GameOverScreen::Draw(ICamera &camera) const
{
    camera.GetDisplayMatrix().DrawSprite(
            *GameOverSprite,
            (int) (GameOverPosition.X),
            (int) (GameOverPosition.Y),
            true);
    camera.GetDisplayMatrix().DrawSprite(
            *PressSprite,
            (int) (PressPosition.X),
            (int) (PressPosition.Y),
            true);
    camera.GetDisplayMatrix().DrawSprite(
            *ButtonSprites[ButtonState],
            (int) (ButtonPosition.X),
            (int) (ButtonPosition.Y),
            true);
}

void GameOverScreen::_loadSprites()
{
    ResourceManager &resources = Game::GetInstance()->Resources;
    GameOverSprite = resources.Sprites["GameOver"];
    PressSprite = resources.Sprites["Press"];
    ButtonSprites[0] = resources.Sprites["BtnA1"];
    ButtonSprites[1] = resources.Sprites["BtnA2"];
}
