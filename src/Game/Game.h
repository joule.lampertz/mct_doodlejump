//
// Created by jlampertz on 17/07/2023.
//

#pragma once


#include "gpio_msp432.h"
#include "spi_msp432.h"
#include "sd_spi_drv.h"
#include "ff.h"
#include "st7735s_drv.h"
#include "uGUI.h"
#include "../Hardware/Joystick.h"
#include "../Hardware/Accelerometer.h"
#include "Logging/Logger.h"
#include "Media/ResourceManager.h"
#include "DisplayMatrix.h"
#include "Level/Level.h"
#include "../Hardware/Button.h"
#include "../Hardware/BackLight.h"

#define SPI_IF  EUSCI_B3_SPI

/**
 * singleton class at the center of the project. it initialises the parts, connects everything and contains the game loop.
 */
class Game
{
private:
    static Game *_instance;

    Game() = default;

// filesystem stuff
    gpio_msp432_pin _cs{PORT_PIN(10, 0)};   // CS Line of SPI interface
    spi_msp432 _spiSD{SPI_IF, _cs};    // SPI interface connected to SD card
    sd_spi_drv _sd{_spiSD};                 // SD card low level driver
    FatFs _fs{_sd};                         // FatFs driver

// display stuff
    gpio_msp432_pin _lcd_rst{PORT_PIN(5, 7)};
    gpio_msp432_pin _lcd_dc{PORT_PIN(3, 7)};
    gpio_msp432_pin _lcd_cs{PORT_PIN(5, 0)};
    spi_msp432 _spi{EUSCI_B0_SPI, _lcd_cs};
    st7735s_drv *_lcd{nullptr};
    uGUI *_gui{nullptr};
    BackLight _backLight{};

// controls
    Accelerometer _accelerometer{14, 13, 11};
    Button _buttonA{5, 1};

    /** whether the game should be restarted at the next opportunity. */
    bool _restart = false;

    /**
     * initialises everything required to render to the display.
     */
    void initDisplay();

    /**
     * initialises all the user inputs and connects them to their handler functions.
     */
    void initControls();

    /**
     * executes one full game cycle
     */
    void gameLoop();

    /**
     * reads user inputs that don't have working interrupts and calls the corresponding handler functions.
     */
    void checkControls();

    /**
     * loads the for the resources that will be used in the game (only sprites so far)
     */
    void loadResources();

    /**
     * draws the current display matrix to the lcd screen.
     */
    void draw();

public:
    /** horizontal number of pixel on the lcd screen. */
    static const unsigned short DISPLAY_WIDTH;

    /** vertical number of pixel on the lcd screen. */
    static const unsigned short DISPLAY_HEIGHT;
    /**
     * logger used to log game events or for debugging <br/>
     * !! outputs have to be set with AddLoggerOutput() !!
     */
    Logger Loggy{};
    /**
     * loads and contains all for the game required resources (only sprites so far)
     */
    ResourceManager Resources{_fs};
    /**
     * a pixel matrix of the size of the lcd display, that contains the information for the next frame.
     */
    DisplayMatrix DM{};

    /**
     * contains all of the level data like environment, player, npc, camera and their behaviour logic.
     */
    Level CurrentLevel{};

    Game(Game const &) = delete;

    void operator=(Game const &) = delete;

    /**
     * creates a new game instance if none exists yet.
     * @return a pointer to the one instance of the singleton game class.
     */
    static Game *GetInstance();

    /**
     * adds an output for the logger to log to.
     * @param output the logger output
     */
    void AddLoggerOutput(ILoggerOutput *output);

    /**
     * initialises all of the for the game required components, like display, resource manager and user input.
     */
    void InitComponents();

    /**
     * does the last steps required to start the game and the runs the game loop forever.
     */
    [[noreturn]]
    void StartGame();

    /**
     * handles accelerometer x axis scan results.
     * @param val accelerometer
     */
    inline void AccelerometerXHandler(float val) const
    {
        CurrentLevel.Player->Velocity.X = val * CurrentLevel.Player->HORIZONTAL_MULTIPLIER;
    }

/**
     * handles button-a push interrupts
     */
    static inline void ButtonAHandler()
    {
        if (Game::GetInstance()->CurrentLevel.GameOver) Game::GetInstance()->Restart();
    }

    /**
     * deletes all with "new" created members of the game class.
     */
    void Clear();

    /**
     * restarts the currently running game.
     */
    void Restart();
};
