//
// Created by jlampertz on 17/07/2023.
//

#include "Game.h"

#include "../Util/Math/Random.h"

Game *Game::_instance = nullptr;
const unsigned short Game::DISPLAY_WIDTH = 128;
const unsigned short Game::DISPLAY_HEIGHT = 128;

Game *Game::GetInstance()
{
    if (_instance == nullptr)
        _instance = new Game;

    return _instance;
}

void Game::InitComponents()
{
    initDisplay();
    initControls();
}

void Game::Clear()
{
    // delete display stuff
    delete _lcd;
    delete _gui;
}

[[noreturn]]
void Game::StartGame()
{
    loadResources();
    Random::SetSeed((unsigned int) std::abs(_accelerometer.GetZ() * 10000));
    while (true)
    {
        CurrentLevel.InitLevel();
        Loggy.Log("game started.", SUCCESS);
        _restart = false;
        while (!_restart) gameLoop(); // while loop game loops aren't elegant, but it works well enough here
        CurrentLevel.Clear();
    }
}

void Game::AddLoggerOutput(ILoggerOutput *output)
{
    Loggy.AddOutput(output);
}

void Game::initDisplay()
{
    // spi inteface
    _spi.setSpeed(24000000);
    // lcd driver
    _lcd = new st7735s_drv{_spi, _lcd_rst, _lcd_dc, st7735s_drv::Crystalfontz_128x128};
    // gui
    _gui = new uGUI{*_lcd};
    _backLight.SetBrightness(1);

    Game::GetInstance()->Loggy.Log("display initialised.", SUCCESS);
}

void Game::loadResources()
{
    const size_t spriteN = 11;
    const char *spritePaths[spriteN]
            {
                    "JumpL",
                    "JumpR",
                    "JumpDL",
                    "JumpDR",
                    "Plat",
                    "PlatApp",
                    "PlatDis",
                    "GameOver",
                    "Press",
                    "BtnA1",
                    "BtnA2",
            };
    Resources.LoadSprites(spritePaths, spriteN);
}

void Game::draw()
{
    _gui->DrawBMP(0, 0, DM.Bitmap);
}

void Game::gameLoop()
{
    checkControls();
    CurrentLevel.UpdateGameObjects();
    CurrentLevel.CheckLoss();
    CurrentLevel.CheckCollision();
    if (!CurrentLevel.GameOver)
    {
        CurrentLevel.DeleteOldPlatforms();
        CurrentLevel.CreateNewPlatforms();
    }
    CurrentLevel.Cam->CaptureScene(CurrentLevel.Objects);

    // draw display matrix to lcd display
    draw();
}

void Game::initControls()
{
    //_joystick.AttachIrqX(JoystickHandler);
    _buttonA.AttachIrq(GPIO::RISING, ButtonAHandler);
}

void Game::checkControls()
{
    // accelerometer irq  didn't work, so i have to check periodically check it myself
    AccelerometerXHandler(_accelerometer.GetX());
}

void Game::Restart()
{
    _restart = true;
}
