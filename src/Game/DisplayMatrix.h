//
// Created by jlampertz on 26/06/2023.
//

#pragma once


#include "Media/Images/ColorRGB565.h"
#include "Media/Images/Sprite.h"
#include "algorithm"

/**
 * the display matrix class represents a 2d RGB565 pixel matrix with an extractable bitmap.
 * it supports sprites with transparent parts being drawn onto it.
 */
class DisplayMatrix
{
public:
    static const unsigned short WIDTH = 128;
    static const unsigned short HEIGHT = 128;
    static const unsigned short BitsPerPixel = 16;

    Color16b _matrix[WIDTH * HEIGHT]{};
    uGUI::BMP *Bitmap;

    /**
     * sets the pixel at a given position (x, y), with (0, 0) being at the left bottom end of the matrix.
     * @param x horizontal position
     * @param y vertical position
     * @param color colour for the pixel
     */
    void setPixel(unsigned short x, unsigned short y, Color16b color);

public:
    /**
     * creates a new object of the pixel matrix class.
     */
    DisplayMatrix();

    /** frees all resources of the object. */
    ~DisplayMatrix();

    /**
     * fills the entire matrix with a specific colour.
     * @param color the colour to fill the matrix with.
     */
    void Fill(Color16b color);

    /**
     * draws a sprite at a a given position (x, y), with (0, 0) being at the left bottom end of the matrix.
     * @param sprite the sprite to draw to the matrix.
     * @param posX horizontal position.
     * @param posY vertical position.
     * @param transparent whether transparent pixel are enabled.
     */
    void DrawSprite(const Sprite &sprite, int posX, int posY, bool transparent);
};
