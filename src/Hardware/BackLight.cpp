//
// Created by jlampertz on 2023-05-02.
//

#include "BackLight.h"
#include "msp.h"
#include "gpio_msp432.h"
#include <limits>

BackLight::BackLight()
{
    // setup light
    gpio_msp432_pin backLight{PORT_PIN(2, 6)};
    backLight.gpioMode(GPIO::OUTPUT);
    backLight.setSEL(1);

    // setup timer a
    TIMER_A0->CTL = TIMER_A_CTL_MC__UP | TIMER_A_CTL_SSEL__SMCLK;   // up mode + clock
    TIMER_A0->CCTL[3] = TIMER_A_CCTLN_OUTMOD_6;                     // toggle/set
    TIMER_A0->CCR[0] = 0;
    TIMER_A0->CCR[3] = 0;
}

void BackLight::SetBrightness(double percent)
{
    double max = std::numeric_limits<unsigned short>::max();
    percent = percent > 1. ? 1. : percent;
    percent = percent < 0 ? 0. : percent;

    TIMER_A0->CCR[0] = static_cast<unsigned short>(max * percent);
    TIMER_A0->CCR[3] = static_cast<unsigned short>(max);
}