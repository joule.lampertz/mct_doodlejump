//
// Created by jlampertz on 25/05/2023.
//

#pragma once

#include "adc14_msp432.h"
#include "msp.h"
#include "gpio_msp432.h"
#include "../Util/Math/Vector2D.h"


/**
 * wraps the adc14_msp432_channel:s of a joystick.
 */
class Joystick
{
private:

    /**
     * handler type for interrupts
     */
    typedef void HANDLER_T(unsigned short val, unsigned short irq_mode);

    adc14_msp432_channel _channelX;
    adc14_msp432_channel _channelY;

    uint16_t _centerX;
    uint16_t _centerY;
    uint16_t _maxRaw;

    /**
     * transforms a scan value with a given center and a range from 0 to a given value, to a range between -1 and 1
     * @param adcScan scan value
     * @param center center of the scan range
     * @param maxRaw max scan value
     * @return value between -1 and 1
     */
    static float _normalize(unsigned short adcScan, unsigned short center, unsigned short maxRaw);

public:

    /**
     * creates a new joystick object
     * @param channelX adc channel number of the x channel of the accelerometer.
     * @param channelY adc channel number of the y channel of the accelerometer.
     */
    Joystick(unsigned short, unsigned short channelY);

    /**
     * reads the value from the x channel and transforms to a range from -1 to 1.
     * @return value from the x channel, transforms to a range from -1 to 1
     */
    Vector2D Get();

    /**
     * reads the value from the x and y channel and transforms both to a range from -1 to 1.
     * @return value from the x and y channels, transformed to a range from -1 to 1
     */
    float GetX();

    /**
     * reads the value from the y channel and transforms to a range from -1 to 1.
     * @return value from the y channel, transforms to a range from -1 to 1
     */
    float GetY();

    /**
     * attaches a handler to the scan interrupt of the x channel
     * @param handler
     */
    void AttachIrqX(const function<void(unsigned short chan, unsigned short value)> &handler);

    /**
     * attaches a handler to the scan interrupt of the y channel
     * @param handler
     */
    void AttachIrqY(const function<void(unsigned short chan, unsigned short value)> &handler);

    /**
     * attaches a handler to the scan interrupts of both channels
     * @param handler
     */
    void AttachIrq(const function<void(unsigned short chan, unsigned short value)> &handler);

    /**
     * makes the current values the new 0.
     */
    void Recenter();

    /**
    * transforms a scan result of the x channel to a value between -1 and 1.
    * @param adcScanX scan result of x channel
    * @return value between -1 and 1
    */
    [[nodiscard]]
    float NormalizeX(unsigned short adcScanX) const;

    /**
    * transforms a scan result of the y channel to a value between -1 and 1.
    * @param adcScanX scan result of y channel
    * @return value between -1 and 1
    */
    [[nodiscard]]
    float NormalizeY(unsigned short adcScanY) const;
};
