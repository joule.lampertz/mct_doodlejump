//
// Created by jlampertz on 2023-05-02.
//

#pragma once

#include "msp.h"
#include "gpio_msp432.h"
#include "task.h"

/**
 * wraps the gpio_msp432_pin of a button.
 */
class Button
{
private:
    gpio_msp432_pin _button;
public:
    /**
     * creates a new button object for a button with a specific gpio pin.
     * @param port port of the button
     * @param pin pin of the button
     */
    Button(unsigned char port, unsigned char pin);

    /**
     * @return whether the button is being pressed or not
     */
    bool Read();

    /**
     * @return whether the button is being pressed or not !!with debounce!!
     */
    bool ReadDebounced();

    /**
     * attach a handler to one of the buttons interrupts.
     * @param irq_mode which interrupt to attach to
     * @param handler
     */
    void AttachIrq(gpio_irq_t irq_mode, const function<void()> &handler);

    inline explicit operator bool() { return this->Read(); }
};
