//
// Created by jlampertz on 27/06/2023.
//

#include <iostream>
#include "ConsoleOutput.h"
#include "posix_io.h"


ConsoleOutput::ConsoleOutput()
{
    // register uart as output
    posix_io::inst.register_stdout(_uart);
    posix_io::inst.register_stdout(_uart);
    posix_io::inst.register_stderr(_uart);
}

void ConsoleOutput::Log(const char *msg, LogLevel level)
{
    switch (level)
    {
        case LogLevel::SUCCESS:
            printf(ANSI_COLOR, GREEN);
            printf("SUCCESS: ");
            break;
        case LogLevel::WARNING:
            printf(ANSI_COLOR, YELLOW);
            printf("WARNING: ");
            break;
        case LogLevel::ERROR:
            printf(ANSI_COLOR, RED);
            printf("ERROR: ");
            break;
        default:
            printf(ANSI_COLOR, DEFAULT);
    }
    printf("%s\n", msg);
    printf(ANSI_COLOR, DEFAULT);
}
