//
// Created by jlampertz on 07/06/2023.
//

#include "SoundPlayer.h"

SoundPlayer::SoundPlayer(unsigned char port, unsigned char pin) : _wobbler(port, pin) {}

void SoundPlayer::SetSound(const Sound &sound)
{
    _sound = sound;
}

void SoundPlayer::Start()
{

}

void SoundPlayer::Stop()
{

}
