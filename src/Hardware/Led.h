#pragma once

#include "msp.h"
#include "gpio_msp432.h"


/**
 * wraps a gpio controller led
 */
class Led
{
private:
    gpio_msp432_pin _led;
public:
    /**
     * creates a new led object for a led with a specific gpio pin
     * @param port port of the led
     * @param pin pin of the led
     */
    Led(unsigned char port, unsigned char pin);

    /**
     * @param active whether the led should be turned on or off
     */
    void Set(bool active);

    /**
     * @return whether the led is currently turned on or off
     */
    bool Get();

    /**
     * toggles the led on or off
     */
    void Toggle();

    inline Led &operator=(bool active)
    {
        this->Set(active);
        return *this;
    }

    inline explicit operator bool() { return this->Get(); }
};
