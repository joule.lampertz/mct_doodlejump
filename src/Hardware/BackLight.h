//
// Created by jlampertz on 2023-05-02.
//

#pragma once

/**
 * class for controlling the brightness of the backlight of the lcd panel
 */
class BackLight
{
public:
    BackLight();

    /**
     * sets the brightness of the backlight
     * @param percent brightness in %. value from 0 to 1
     */
    void SetBrightness(double percent);
};
