//
// Created by jlampertz on 07/06/2023.
//

#pragma once


#include "FrequencyPlayer.h"
#include "../Game/Media/Sound/Sound.h"

/**
 * this class is supposed to combine frequencies into complex sounds and play them
 * buttttt it doesn't work yet and would require some major rewriting to work :<
 */
class SoundPlayer
{
private:
    FrequencyPlayer _wobbler;
    Sound _sound;

public:
    SoundPlayer(unsigned char port, unsigned char pin);

    void SetSound(const Sound &sound);

    void Start();

    void Stop();
};
