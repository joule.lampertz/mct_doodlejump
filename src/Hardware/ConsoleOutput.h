//
// Created by jlampertz on 27/06/2023.
//

#pragma once


#include "../Game/Logging/ILoggerOutput.h"
#include "uart_msp432.h"

#define ANSI_COLOR "%c[%dm",27

/**
 * a logger output that logs to the uart connected console.
 */
class ConsoleOutput : public ILoggerOutput
{

private:
    const int DEFAULT = 0;
    const int BLACK = 30;
    const int RED = 31;
    const int GREEN = 32;
    const int YELLOW = 33;
    const int BLUE = 34;
    const int MAGENTA = 35;
    const int CYAN = 36;
    const int WHITE = 37;


    uart_msp432 _uart{};
public:
    ConsoleOutput();

    /**
     * logs a message to the console with some additional formatting
     * @param msg message to log
     * @param level log level (decides formatting)
     */
    void Log(const char *msg, LogLevel level) override;
};
