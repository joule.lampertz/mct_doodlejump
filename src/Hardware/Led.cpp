//
// Created by jlampertz on 19/05/2023.
//

#include "Led.h"

Led::Led(unsigned char port, unsigned char pin) : _led(PORT_PIN(port, pin))
{
    _led.gpioMode(GPIO::OUTPUT);
}

bool Led::Get() { return _led.gpioRead(); }

void Led::Set(bool active) { _led.gpioWrite(active); }

void Led::Toggle() { _led.gpioToggle(); }
