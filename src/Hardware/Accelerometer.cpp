//
// Created by jlampertz on 25/05/2023.
//

#include <limits>
#include <cstdio>
#include "Accelerometer.h"

Accelerometer::Accelerometer(unsigned short channelX, unsigned short channelY, unsigned short channelZ)
        : _channelX(channelX), _channelY(channelY), _channelZ(channelZ)
{
    _channelX.adcMode(ADC::ADC_10_BIT);
    _channelY.adcMode(ADC::ADC_10_BIT);
    _channelZ.adcMode(ADC::ADC_10_BIT);

    adc14_msp432 &adc = adc14_msp432::inst;
    adc.adcSetupScan(ADC::ADC_10_BIT);

    auto minMax = std::minmax({channelX, channelY, channelZ});
    adc.adcStartScan(minMax.first, minMax.second);

    _maxRaw = 1023;

    Recenter();
}

float Accelerometer::GetX()
{
    return NormalizeX(_channelX.adcReadScan());
}

float Accelerometer::GetY()
{
    return NormalizeY(_channelY.adcReadScan());
}

float Accelerometer::GetZ()
{
    return NormalizeZ(_channelZ.adcReadScan());
}

void Accelerometer::AttachIrq(const function<void(unsigned short, unsigned short value)> &handler)
{
    AttachIrqX(handler);
    AttachIrqY(handler);
    AttachIrqZ(handler);
}

void Accelerometer::AttachIrqX(const function<void(unsigned short, unsigned short)> &handler)
{
    _channelX.attachScanIrq(handler.target<HANDLER_T>());
}

void Accelerometer::AttachIrqY(const function<void(unsigned short, unsigned short)> &handler)
{
    _channelY.attachScanIrq(handler.target<HANDLER_T>());
}

void Accelerometer::AttachIrqZ(const function<void(unsigned short, unsigned short)> &handler)
{
    _channelZ.attachScanIrq(handler.target<HANDLER_T>());
}

void Accelerometer::Recenter()
{
    _centerX = _channelX.adcReadScan();
    _centerY = _channelY.adcReadScan();
    _centerZ = _channelZ.adcReadScan();
}

float Accelerometer::NormalizeX(unsigned short adcScanX) const
{
    return _normalize(adcScanX, _centerX, _maxRaw);
}

float Accelerometer::NormalizeY(unsigned short adcScanY) const
{
    return _normalize(adcScanY, _centerY, _maxRaw);
}

float Accelerometer::NormalizeZ(unsigned short adcScanZ) const
{
    return _normalize(adcScanZ, _centerZ, _maxRaw);
}

float Accelerometer::_normalize(unsigned short adcScan, unsigned short center, unsigned short maxRaw)
{
    if (adcScan < center)
    {
        return (static_cast<float>(adcScan) / static_cast<float>(center)) - 1;
    } else
    {
        return (static_cast<float>(adcScan - center) / static_cast<float>(maxRaw - center));
    }
}
