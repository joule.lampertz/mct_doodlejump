//
// Created by jlampertz on 07/06/2023.
//

#pragma once


#include "gpio_msp432.h"
#include "timer_msp432.h"

/**
 * class to play specific frequencies on the buzzer or other gpio controller devices
 */
class FrequencyPlayer
{
private:
    gpio_msp432_pin _speaker;
    timer_msp432 _timer;
    bool _playing;
    unsigned int _frequency;

public:
    /**
     * creates a new frequency player for a specific gpio pin
     * @param port
     * @param pin
     */
    FrequencyPlayer(unsigned char port, unsigned char pin);

    /**
     * @param hz the frequency to be played on the gpio pin (in hz)
     */
    void SetFrequency(unsigned int hz);

    /**
     * @return the currently set frequency
     */
    unsigned int GetFrequency();

    /**
     * starts playing the set frequency.
     */
    void Start();

    /**
     * stops playing the frequency.
     */
    void Stop();

    /**
     * whether the player is currently playing a frequency.
     * @return
     */
    bool IsPlaying();
};
