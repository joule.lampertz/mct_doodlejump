//
// Created by jlampertz on 2023-05-02.
//

#include "Button.h"

Button::Button(unsigned char port, unsigned char pin) : _button(PORT_PIN(port, pin))
{
    _button.gpioMode(GPIO::INPUT | GPIO::PULLUP);
}

bool Button::Read() { return !_button.gpioRead(); }

bool Button::ReadDebounced()
{
    if (this->Read())
    {
        task::sleep(20);
        if (!this->Read())
        {
            task::sleep(20);
            return true;
        }
    }
    return false;
}

void Button::AttachIrq(gpio_irq_t irq_mode, const function<void()> &handler)
{
    _button.gpioAttachIrq(irq_mode, handler);
}