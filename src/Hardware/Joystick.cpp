//
// Created by jlampertz on 25/05/2023.
//

#include <limits>
#include <cstdio>
#include "Joystick.h"

Joystick::Joystick(uint16_t channelX, uint16_t channelY) : _channelX(channelX), _channelY(channelY)
{
    _channelX.adcMode(ADC::ADC_10_BIT);
    _channelY.adcMode(ADC::ADC_10_BIT);

    adc14_msp432 &adc = adc14_msp432::inst;
    adc.adcSetupScan(ADC::ADC_10_BIT);

    if (channelX < channelY)
    {
        adc.adcStartScan(channelX, channelY);
    } else
    {
        adc.adcStartScan(channelY, channelX);
    }

    _maxRaw = 1023;

    Recenter();
}

float Joystick::GetX()
{
    return NormalizeX(_channelX.adcReadScan());

}

float Joystick::GetY()
{
    return NormalizeY(_channelY.adcReadScan());
}

void Joystick::AttachIrq(const function<void(unsigned short, unsigned short value)> &handler)
{
    AttachIrqX(handler);
    AttachIrqY(handler);
}

void Joystick::AttachIrqX(const function<void(unsigned short, unsigned short)> &handler)
{
    _channelX.attachScanIrq(handler.target<HANDLER_T>());
}

void Joystick::AttachIrqY(const function<void(unsigned short, unsigned short)> &handler)
{
    _channelY.attachScanIrq(handler.target<HANDLER_T>());
}

void Joystick::Recenter()
{
    _centerX = _channelX.adcReadScan();
    _centerY = _channelY.adcReadScan();
}

float Joystick::NormalizeY(unsigned short adcScanY) const
{
    return _normalize(adcScanY, _centerY, _maxRaw);
}

float Joystick::NormalizeX(unsigned short adcScanX) const
{
    return _normalize(adcScanX, _centerX, _maxRaw);
}

float Joystick::_normalize(unsigned short adcScan, unsigned short center, unsigned short maxRaw)
{
    if (adcScan < center)
    {
        return (static_cast<float>(adcScan) / static_cast<float>(center)) - 1;
    } else
    {
        return (static_cast<float>(adcScan - center) / static_cast<float>(maxRaw - center));
    }
}

Vector2D Joystick::Get()
{
    return {GetX(), GetY()};
}
