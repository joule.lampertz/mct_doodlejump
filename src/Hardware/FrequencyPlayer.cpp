//
// Created by jlampertz on 07/06/2023.
//

#include "FrequencyPlayer.h"

FrequencyPlayer::FrequencyPlayer(unsigned char port, unsigned char pin) : _speaker(PORT_PIN(port, pin))
{
    // set up speaker
    _speaker.gpioMode(GPIO::OUTPUT);

    // set up timer
    _timer.setCallback([this]()
                       {
                           this->_speaker.gpioToggle();
                       });
}

void FrequencyPlayer::SetFrequency(unsigned int hz)
{
    _frequency = hz;
    if (hz <= 0 && _playing)
    {
        _timer.stop();
        return;
    }

    _timer.setPeriod(500000 / _frequency, TIMER::PERIODIC); // 500000ns == 0.5s
    if (_playing) _timer.start();
}

unsigned int FrequencyPlayer::GetFrequency()
{
    return _frequency;
}

void FrequencyPlayer::Start()
{
    _playing = true;
    if (_frequency != 0 && !_timer.isRunning()) _timer.start();
}

void FrequencyPlayer::Stop()
{
    _playing = false;
    if (_timer.isRunning()) _timer.stop();
}

bool FrequencyPlayer::IsPlaying()
{
    return _playing;
}


