$files = Get-ChildItem ".\png\" -Filter "*.png"

for ($i=0; $i -lt $files.Count; $i++) {
    $outfile = ".\bm\" + $files[$i].BaseName + ".bm" 
    Write-Host "in: " + $files[$i].FullName
    Write-Host "out: " + $outfile
	.\png2cpp -r -o $outfile $files[$i].FullName
}

Start-Sleep -Seconds 3